<?php
namespace Mageplaza\Affiliate\Model;
class History extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'affiliate_history';

    protected $_cacheTag = 'affiliate_history';

    protected $_eventPrefix = 'affiliate_history';

    protected function _construct()
    {
        $this->_init('Mageplaza\Affiliate\Model\ResourceModel\History');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}
