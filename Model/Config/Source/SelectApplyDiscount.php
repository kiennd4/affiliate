<?php
namespace Mageplaza\Affiliate\Model\Config\Source;
use Magento\Framework\Option\ArrayInterface;
class SelectApplyDiscount implements ArrayInterface
{
    /**
     * Retrieve option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => '1', 'label' => __('No')],
            ['value' => '2', 'label' => __('Fixed Value')],
            ['value' => '3', 'label' => __('Percentage of order total')],
        ];
    }
}
