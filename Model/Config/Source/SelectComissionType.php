<?php
namespace Mageplaza\Affiliate\Model\Config\Source;
use Magento\Framework\Option\ArrayInterface;
class SelectComissionType implements ArrayInterface
{
    /**
     * Retrieve option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => '1', 'label' => __('Fixed Value')],
            ['value' => '2', 'label' => __('Percentage of order total')]
        ];
    }
}
