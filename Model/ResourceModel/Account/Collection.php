<?php

namespace Mageplaza\Affiliate\Model\ResourceModel\Account;

/* use required classes */

use Magento\Framework\Data\Collection\EntityFactoryInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'account_id'; //primary id of the table

    /**
     * @param EntityFactoryInterface $entityFactory ,
     * @param LoggerInterface $logger ,
     * @param FetchStrategyInterface $fetchStrategy ,
     * @param ManagerInterface $eventManager ,
     * @param StoreManagerInterface $storeManager ,
     * @param AdapterInterface $connection ,
     * @param AbstractDb $resource
     */
    public function __construct(
        EntityFactoryInterface $entityFactory,
        LoggerInterface $logger,
        FetchStrategyInterface $fetchStrategy,
        ManagerInterface $eventManager,
        StoreManagerInterface $storeManager,
        AdapterInterface $connection = null,
        AbstractDb $resource = null
    )
    {
        $this->_init('Mageplaza\Affiliate\Model\Account', 'Mageplaza\Affiliate\Model\ResourceModel\Account');

        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
        $this->storeManager = $storeManager;
    }

    protected function _initSelect()
    {
        parent::_initSelect();

        $this->getSelect()->joinInner(
            ['customer_entity' => $this->getTable('customer_entity')], //2nd table name by which you want to join mail table
            '`main_table`.customer_id= `customer_entity`.entity_id', // common column which available in both table
            ['customer_entity.lastname', 'customer_entity.email'] // '*' define that you want all column of 2nd table. if you want some particular column then you can define as ['column1','column2']
        );
    }
}
