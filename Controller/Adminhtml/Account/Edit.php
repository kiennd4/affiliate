<?php
namespace Mageplaza\Affiliate\Controller\Adminhtml\Account;
use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
class Edit extends Action
{
    protected $request;
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Mageplaza\Affiliate\Model\AccountFactory $accountFactory
    ) {
        parent::__construct($context);
        $this->request = $request;
        $this->coreRegistry = $coreRegistry;
        $this->accountFactory = $accountFactory;
    }
    public function getIddata()
    {
        // use
        $this->request->getParams();
        return $this->request->getParam('id');
    }
    public function execute()
    {
        $keyId = $this->getIddata();
        $accountId = $this->getRequest()->getParam('account_id');
        if ($keyId) {
            $accountModel = $this->accountFactory->create()->load($accountId);
            $this->coreRegistry->register('account_action', $accountModel);
            $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
            $resultPage->getConfig()->getTitle()->prepend(__("Edit Account"));
        }else {
            $account = $this->accountFactory->create()->load('account_id');
            $this->coreRegistry->register('account_action', $account);
            $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
            $resultPage->getConfig()->getTitle()->prepend(__("New Account"));
        }
        return $resultPage;
    }

}
