<?php
namespace Mageplaza\Affiliate\Controller\Adminhtml\Account;
use Magento\Backend\App\Action;
use Magento\Backend\Model\Auth\Session;
use PharIo\Version\Exception;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $_adminSession;
    /**
     * @var \Mageplaza\Affiliate\Model\AccountFactory
     */
    protected $_accountAffiliateFactory;
    protected $resultFactory;
    protected $historyFactory;
    protected $scopeConfig;
    protected $_customerFactory;
    /**
     * @param Action\Context                      $context
     * @param \Magento\Backend\Model\Auth\Session $adminSession
     * @param \Mageplaza\Affiliate\Model\AccountFactory $accountFactory
     * @param \Mageplaza\Affiliate\Model\HistoryFactory $historyFactory
     */
    protected $request;


    public function __construct(
        Action\Context $context,
        \Mageplaza\Affiliate\Controller\Adminhtml\Account\Edit $paramId,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Backend\Model\Auth\Session $adminSession,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Mageplaza\Affiliate\Model\AccountFactory $accountFactory,
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Mageplaza\Affiliate\Model\HistoryFactory $historyFactory
    ) {
        parent::__construct($context);
        $this->_adminSession = $adminSession;
        $this->_accountAffiliateFactory = $accountFactory;
        $this->_customerFactory = $customerFactory;
        $this->historyFactory = $historyFactory;
        $this->resultFactory = $resultFactory;
        $this->scopeConfig = $scopeConfig;
        $this->request = $request;
    }
    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */

    protected $newCode;
    public function getGiftCode($length) {
        return $this->newCode = substr(str_shuffle('abcdefghijklmnopqrstuvxyz0123456789'),0 , $length);
    }

    public function execute()
    {
        $length = $this->scopeConfig->getValue('affiliate/general/length',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $affiliateModel = $this->_accountAffiliateFactory->create();
            $historyModel = $this->historyFactory->create();
            $accountId = $this->getRequest()->getParam('account_id');
            $customer_id = $this->getRequest()->getParam(('customer_id'));
            $customerData = $this->_customerFactory->create()->load($customer_id)->getData();
            $affiliateData = $affiliateModel->load($customer_id, 'customer_id');
            $balance = $this->getRequest()->getParam('balance');

            if(($affiliateData->getData() != null || $customerData == null) && $accountId == null){
                $this->messageManager->addError('Customer had affiliate account or Customer don\'t exist');
                $redirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
                $redirect->setUrl($this->_redirect->getRefererUrl());
                return $redirect;
            }

            if ($accountId) {
                $affiliateModel->load($accountId);
            }else{
                $data['code'] = $this->getGiftCode($length);
            }
            $affiliateModel->setData($data);

            try {
                $affiliateModel->save();
                if($balance > 0) {
                    $historyData = [
                        'customer_id' => $affiliateModel->load($accountId)->getData('customer_id'),
                        'is_admin_change' => '1',
                        'amount' => $this->getRequest()->getParam('balance'),
                        'status' => $this->getRequest()->getParam('status')
                    ];
                    $historyModel->setData($historyData);
                }
                $historyModel->save();
                $this->messageManager->addSuccess(__('The data has been saved.'));
                $this->_adminSession->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('affiliate/*/newaccount', ['id' => $affiliateModel->getId(), '_current' => true, 'data' => $data]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
