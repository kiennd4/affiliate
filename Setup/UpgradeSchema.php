<?php

namespace Mageplaza\Affiliate\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            if (!$installer->tableExists('affiliate_history')) {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable('affiliate_history')
                )
                    ->addForeignKey(
                        $installer->getFkName('affiliate_account', 'customer_id', 'affiliate_history', 'customer_id'),
                        'customer_id',
                        $installer->getTable('affiliate_history'),
                        'customer_id',
                        \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
                    );
                $installer->getConnection()->createTable($table);
                $installer->getConnection()->addIndex(
                    $installer->getTable('affiliate_history'),
                    $setup->getIdxName(
                        $installer->getTable('affiliate_history'),
                        ['customer_id','history_id'],
                        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_INDEX
                    ),
                    ['customer_id','history_id'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_INDEX
                );
                $installer->getConnection()->addIndex(
                    $installer->getTable('affiliate_account'),
                    $setup->getIdxName(
                        $installer->getTable('affiliate_account'),
                        ['customer_id'],
                        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_INDEX
                    ),
                    ['customer_id'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_INDEX
                );
            }
        }

        $installer->endSetup();
    }
}
