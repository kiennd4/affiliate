<?php

namespace Mageplaza\Affiliate\Plugin;
class CheckReferPlugin
{
    protected $_checkoutSession;
    public function __construct(
        \Magento\Checkout\Model\Session             $checkoutSession
    )
    {
        $this->_checkoutSession = $checkoutSession;
    }
    public function afterGetCouponCode(\Magento\Checkout\Block\Cart\Coupon $subject, $result)
    {
        $model = $this->_checkoutSession->getData('refer_code');
        if ($model) {
            return $model;
        }else{
            return $result;
    }
    }
}
