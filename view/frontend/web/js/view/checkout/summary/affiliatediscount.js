define(
    [
        'jquery',
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/quote'
    ],
    function ($,Component,quote) {
        "use strict";
        return Component.extend({
            defaults: {
                template: 'Mageplaza_Affiliate/checkout/summary/affiliatediscount'
            },
            quoteIsVirtual: quote.isVirtual(),
            totals: quote.getTotals(),
            isDisplayedAffiliatediscount : function(){
                var segments = this.totals()['total_segments'];
                for( let i = 0; i< segments.length; i++){
                    var code = segments[i];
                    if(code['code'] === 'affiliate_discount' && code['value'] !== 0 && code['value'] !== null){
                        return true;
                    }
                }
                return false;
            },
            getAffiliateDiscount : function(){
                var segments = this.totals()['total_segments'];
                for(var i = 0; i < segments.length; i++) {
                    var total = segments[i];
                    if (total['code'] === 'affiliate_discount'){
                        var price =  total['value'];
                        return this.getFormattedPrice(price);
                    }
                }
            }
        });
    }
);
