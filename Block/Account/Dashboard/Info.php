<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Mageplaza\Affiliate\Block\Account\Dashboard;

use Magento\Framework\NumberFormatter;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;


class Info extends Template
{

    protected $_customer;
    protected $_storeManager;
    protected $_customerFactory;
    protected $_accountFactory;
    protected $_historyFactory;
    protected $_customerSession;
    /**
     * Constructor
     *
     * @param Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param array $data
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Customer $customers,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Mageplaza\Affiliate\Model\AccountFactory $accountFactory,
        \Mageplaza\Affiliate\Model\HistoryFactory $historyFactory,
        array $data = []
    ) {
        $this->_storeManager = $storeManager;
        $this->_customerSession = $customerSession;
        $this->_customerFactory = $customerFactory;
        $this->_customer = $customers;
        $this->_accountFactory = $accountFactory;
        $this->_historyFactory = $historyFactory;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    public function getKey(){
        $key = $this->scopeConfig->getValue('affiliate/general/url_key',
            ScopeInterface::SCOPE_STORE);
        return $key;
    }

    public function formatPrice($number) {
        $formatter = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
        return $formatter->formatCurrency($number, 'USD');
    }

    public function getAccountData(){
        $data = $this->_accountFactory->create();
        return $data->load($this->_customerSession->getCustomer()->getId(), 'customer_id');
    }

    public function getCode(){
          $code = $this->getAccountData()->getData('code');
        return $code;
    }

    public function getStatus($status){
        if($status==0){
            return 'Inactive';
        }else{
            return 'Active';
        }
    }

    public function getTitle($title, $orderId){
        if($title==0 && $orderId != 0) {
            return 'Created from order #' . $orderId;
        }else{
            return 'Changed by Admin';
        }
    }

    public function getBalance() {
        $number = $this->getAccountData()->getData('balance');
        if($number){
            return $this->formatPrice($number);
        }
    }

    public function getHistory() {
        $history = $this->_historyFactory->create()->getCollection()->addFieldToFilter('customer_id', $this->_customerSession->getCustomer()->getId());
        $data = $history->getData();
        return $data;
    }

    public function getBlock(){
        $block = $this->scopeConfig->getValue('affiliate/general/select',
            ScopeInterface::SCOPE_STORE);
        return $block;
    }

    public function getBaseUrl(){
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    public function getCustomLink(){
        $urlLink = $this->getBaseUrl() . "affiliate/account/register";
        return $urlLink;
    }

    public function checkBlock(){
        if($this->getAccountData()->getData() != null){
            return 1;
        }else{
            return 0;
        }
    }
}
