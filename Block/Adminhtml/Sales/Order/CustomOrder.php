<?php
namespace Mageplaza\Affiliate\Block\Adminhtml\Sales\Order;

use Magento\Sales\Model\Order;

class CustomOrder extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Order
     */
    protected $_order;
    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_source;
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    protected $_custom;
    protected $_historyFactory;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Mageplaza\Affiliate\Model\Total\Quote\Custom $custom,
        \Mageplaza\Affiliate\Model\HistoryFactory $historyFactory,
        array $data = []
    ) {
        $this->_custom = $custom;
        $this->_historyFactory = $historyFactory;
        parent::__construct($context, $data);
    }
    public function getSource()
    {
        return $this->_source;
    }

    public function getConfig($path){
        return $this->_scopeConfig->getValue($path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function displayFullSummary()
    {
        return true;
    }
    public function initTotals()
    {
        $parent = $this->getParentBlock();
        $this->_order = $parent->getOrder();
        $this->_source = $parent->getSource();
        $history = $this->_historyFactory->create()->load( $this->_order->getData('quote_id'),'order_id');
        if($history->getData('order_increment_id')==$this->_order->getData('increment_id')){
            $customAmount = new \Magento\Framework\DataObject(
                [
                    'code' => 'affiliate_discount',
                    'strong' => false,
                    'value' => -$this->_custom->getCookie('discount'),
                    'label' => __('Affiliate Discount'),
                ]
            );
            $parent->addTotal($customAmount, 'affiliate_discount');
        }
        if($history->getData('order_increment_id')==$this->_order->getData('increment_id')){
            $customAmount = new \Magento\Framework\DataObject(
                [
                    'code' => 'commission',
                    'strong' => false,
                    'value' => $this->_custom->getCookie('commission'),
                    'label' => __('Commission'),
                ]
            );
            $parent->addTotal($customAmount, 'commission');
        }
        return $this;
    }
    /**
     * Get order store object
     *
     * @return \Magento\Store\Model\Store
     */
    public function getStore()
    {
        return $this->_order->getStore();
    }
    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->_order;
    }
    /**
     * @return array
     */
    public function getLabelProperties()
    {
        return $this->getParentBlock()->getLabelProperties();
    }
    /**
     * @return array
     */
    public function getValueProperties()
    {
        return $this->getParentBlock()->getValueProperties();
    }
}
