<?php

namespace Mageplaza\Affiliate\Observer;

use Magento\Framework\App\Action\Action;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\ActionFlag;

class CheckReferCode implements ObserverInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    protected $_messageManager;

    protected $_actionFlag;

    protected $_customerSession;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;

    protected $_accountFactory;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */

    protected $_checkoutSession;
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\ActionFlag $actionFlag,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Mageplaza\Affiliate\Model\AccountFactory $accountFactory
    )
    {
        $this->_messageManager = $messageManager;
        $this->_actionFlag = $actionFlag;
        $this->redirect = $redirect;
        $this->_checkoutSession = $checkoutSession;
        $this->_customerSession = $customerSession;
        $this->_accountFactory = $accountFactory;
    }

    /**
     * customer register event handler
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        // get enetered gift code
        $controller = $observer->getControllerAction();
        /** @var Action $controller */
        $controller->getRequest()->getControllerName();
        $couponCode = $controller->getRequest()->getParam('coupon_code');
        $affiliateModel = $this->_accountFactory->create();
        $referCode = $affiliateModel->load($couponCode, 'code');
        $customerId = $this->_customerSession->getCustomer()->getId();
        $checkReferCode = $this->_accountFactory->create()->load($customerId,'customer_id')->getData('code') === $couponCode;
        if(!empty($referCode->getData('code')) && $checkReferCode == false) {
            $this->_checkoutSession->setData('refer_code', $couponCode);
            $this->_checkoutSession->setData('discount', $referCode->getData('balance'));
            $this->_messageManager->addSuccessMessage(__("Refer code applied successfully"));
            $this->_actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
            $this->redirect->redirect($controller->getResponse(), 'checkout/cart');
            return;
        }

        if ($controller->getRequest()->getParam('remove') == 1) {
            $this->_checkoutSession->setData('refer_code', '');
            $this->_checkoutSession->setData('discount', '');
        }
    }
}
