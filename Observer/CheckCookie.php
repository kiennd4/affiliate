<?php

namespace Mageplaza\Affiliate\Observer;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Event\ObserverInterface;
use Mageplaza\Affiliate\Model\AccountFactory;

class CheckCookie implements ObserverInterface
{
    protected $_scopeConfig;
    protected $_accountFactory;
    protected $_customerSession;
    private $customCookieManager;
    private $customCookieMetadataFactory;

    public function __construct(
        Context $context,
        \Magento\Framework\Stdlib\CookieManagerInterface $customCookieManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Mageplaza\Affiliate\Model\AccountFactory $accountFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $customCookieMetadataFactory
    )
    {
        $this->customCookieMetadataFactory = $customCookieMetadataFactory;
        $this->customCookieManager = $customCookieManager;
        $this->_customerSession = $customerSession;
        $this->_accountFactory = $accountFactory;
        $this->_scopeConfig = $scopeConfig;
    }

    public function getConfig($path){
        return $this->_scopeConfig->getValue($path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getCookie($key)
    {
        return $this->customCookieManager->getCookie(
            $key
        );
    }

    public function deleteCookie($key,$metadata){
        return $this->customCookieManager->deleteCookie(
            $key,$metadata
        );
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customerId = $this->_customerSession->getCustomer()->getId();
        $customerCode = $this->_accountFactory->create()->load($customerId, 'customer_id')->getData('code');
        $key = $this->getConfig('affiliate/general/url_key');
        if($customerCode === $this->getCookie($key)){
            $metadata = $this->customCookieMetadataFactory->createPublicCookieMetadata();
            $metadata->setPath('/');
            $this->deleteCookie($key,$metadata);
        }
    }
}
